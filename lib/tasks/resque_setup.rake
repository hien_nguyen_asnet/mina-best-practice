# Resque tasks
require 'resque/tasks'
require 'resque/scheduler/tasks'
require 'resque/pool/tasks'

namespace :resque do
  task :setup => :environment do
    require 'resque'
    require 'resque-scheduler'

    # you probably already have this somewhere
    # TODO: This should be stored in .env file
    Resque.redis = 'localhost:6379'

    # If you want to be able to dynamically change the schedule,
    # uncomment this line.  A dynamic schedule can be updated via the
    # Resque::Scheduler.set_schedule (and remove_schedule) methods.
    # When dynamic is set to true, the scheduler process looks for
    # schedule changes and applies them on the fly.
    # Note: This feature is only available in >=2.0.0.
    # Resque::Scheduler.dynamic = true

    # The schedule doesn't need to be stored in a YAML, it just needs to
    # be a hash.  YAML is usually the easiest.
    Resque.schedule = YAML.load_file('config/resque_schedule.yml')
    Dir[File.join(Rails.root, 'app', 'jobs', '*.rb')].each { |file| require file }
  end

  task "resque:pool:setup" do
    # close any sockets or files in pool manager
    ActiveRecord::Base.connection.disconnect!
    # and re-open them in the resque worker parent
    Resque::Pool.after_prefork do |job|
      ActiveRecord::Base.establish_connection
      Resque.redis.client.reconnect
    end
  end
end