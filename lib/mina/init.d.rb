def setup_service(service_name, pid_file, control_template, log_file = nil)
  queue "echo '-----> Setup #{service_name} service'"
  upload_service_template service_name, pid_file, log_file
  add_control_service service_name, control_template
  symlink_service service_name
end

def upload_service_template(service_name, pid_file, log_file = nil)
  set :service_name, service_name
  set :pid_file, pid_file
  # Upload bash script start service
  upload_template "#{service_name} service", "service", "#{deploy_to}/#{shared_path}/config/#{service_name}"
  queue echo_cmd "sudo chmod +x #{deploy_to}/#{shared_path}/config/#{service_name}"
end

def symlink_service(service_name)
  queue echo_cmd "sudo rm /etc/init.d/#{service_name}"
  queue echo_cmd "sudo ln -s #{deploy_to}/#{shared_path}/config/#{service_name} /etc/init.d/#{service_name}"
end

def add_control_service(service_name, control_template)
  upload_template "#{service_name} control template", "#{control_template}", "#{deploy_to}/#{shared_path}/config/#{control_template}"
  queue echo_cmd "cat #{deploy_to}/#{shared_path}/config/#{control_template} >> #{deploy_to}/#{shared_path}/config/#{service_name}"
  queue echo_cmd "rm #{deploy_to}/#{shared_path}/config/#{control_template}"
end

def start_service(service_name)
  queue echo_cmd "service #{service_name} start"
end

def stop_service(service_name)
  queue echo_cmd "service #{service_name} stop"
end

def restart_service(service_name)
  queue echo_cmd "service #{service_name} restart"
end

def register_service(service_name)
  queue echo_cmd "sudo update-rc.d -f #{service_name} remove"
  queue echo_cmd "sudo update-rc.d #{service_name} defaults"
end

def remove_service(service_name)
  queue echo_cmd "sudo update-rc.d -f #{service_name} remove"
end

def uninstall_service(service_name)
  queue echo_cmd "service #{service_name} uninstall"
end
