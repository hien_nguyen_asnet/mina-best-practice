###########################################################################
# Unicorn Tasks
###########################################################################

namespace :unicorn do
  desc "Upload and update (link) all Unicorn config files"
  task :update => [:upload]

  desc "Parses all Unicorn config files and uploads them to server"
  task :upload => [:'upload:config']

  namespace :upload do
    desc "Parses Unicorn config file and uploads it to server"
    task :config do
      upload_template 'Unicorn config', 'unicorn.rb', "#{config_path}/unicorn.rb"
    end
  end

  desc "Parses all Unicorn config files and shows them in output"
  task :parse => [:'parse:config', :'parse:script']

  namespace :parse do
    desc "Parses Unicorn config file and shows it in output"
    task :config do
      puts "#"*80
      puts "# unicorn.rb"
      puts "#"*80
      puts erb("#{config_templates_path}/unicorn.rb.erb")
    end
  end

  desc "Setup unicorn service"
  task :setup => :environment do
    setup_service("unicorn", "#{unicorn_pid}", "unicorn_control")
  end

  desc "Register unicorn service start with machine"
  task :register => :environment do
    register_service("unicorn")
  end

  desc "Start unicorn service"
  task :start => :environment do
    start_service("unicorn")
  end

  desc "Restart unicorn service"
  task :restart => :environment do
    restart_service("unicorn")
  end

  desc "Stop unicorn service"
  task :stop => :environment do
    stop_service("unicorn")
  end

  desc "Remove unicorn service"
  task :remove => :environment do
    remove_service("unicorn")
  end
end
