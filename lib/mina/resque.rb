###########################################################################
# Resque Tasks
###########################################################################

namespace :resque do
  desc "Setup resque service"
  task :setup do
    invoke :'resque:pool:setup'
    invoke :'resque:scheduler:setup'
  end
  desc "Register resque service"
  task :register do
    invoke :'resque:pool:register'
    invoke :'resque:scheduler:register'
  end
  desc "Start resque service"
  task :start do
    invoke :'resque:pool:start'
    invoke :'resque:scheduler:start'
  end
  desc "Restart resque service"
  task :restart do
    invoke :'resque:pool:restart'
    invoke :'resque:scheduler:restart'
  end
  desc "Stop resque service"
  task :stop do
    invoke :'resque:pool:stop'
    invoke :'resque:scheduler:stop'
  end

  namespace :pool do
    desc "Setup resque pool service"
    task :setup => :environment do
      # setup_service("resque_pool", "resque-pool.sh", "resque-stop.sh")
      setup_service("resque_pool", "#{resque_pool_pid}", "resque_pool_control")
      # invoke :'resque:pool:upload'
    end
    desc "Register resque pool service start with machine"
    task :register => :environment do
      register_service("resque_pool")
    end
    desc "Start resque pool service"
    task :start => :environment do
      start_service("resque_pool")
    end

    desc "Restart resque pool service"
    task :restart => :environment do
      restart_service("resque_pool")
    end

    desc "Stop resque pool service"
    task :stop => :environment do
      stop_service("resque_pool")
    end
  end

  namespace :scheduler do
    desc "Setup resque scheduler service"
    task :setup => :environment do
      # setup_service("resque_scheduler", "resque-scheduler.sh", "resque-stop.sh")
      setup_service("resque_scheduler", "#{resque_scheduler_pid}", "resque_scheduler_control")
    end
    desc "Register resque scheduler service start with machine"
    task :register => :environment do
      register_service("resque_scheduler")
    end
    desc "Start resque scheduler service"
    task :start => :environment do
      start_service("resque_scheduler")
    end
    desc "Restart resque scheduler service"
    task :restart => :environment do
      restart_service("resque_scheduler")
    end
    desc "Stop resque scheduler service"
    task :stop => :environment do
      stop_service("resque_scheduler")
    end

    task :remove => :environment do
      remove_service("resque_scheduler")
    end
  end
end
