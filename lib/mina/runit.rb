namespace :runit do
  task :setup => :environment do
    queue 'echo "-----> Create service path"'
    queue echo_cmd "sudo mkdir -p #{runit_service_path} && sudo chown #{user}:#{group} #{runit_service_path}"
  end
end

def setup_service(service_name, template, stop_template = nil)
  queue 'echo "-----> Create #{service_name} service path"'
  queue echo_cmd "mkdir -p #{runit_service_path}/#{service_name}"
  upload_service_template service_name, template, stop_template
  queue echo_cmd "sudo rm /etc/service/#{service_name}"
  queue echo_cmd "sudo ln -s #{runit_service_path}/#{service_name} /etc/service/#{service_name}"
end

def upload_service_template(service_name, template, stop_template = nil)
  # Upload bash script start service
  upload_template "#{service_name} start service", "#{template}", "#{runit_service_path}/#{service_name}/#{template}"
  queue echo_cmd "chmod +x #{runit_service_path}/#{service_name}/#{template}"

  # Upload run file runit service
  set :runit_service_start, "#{runit_service_path}/#{service_name}/#{template}"
  upload_template 'resque-pool run service file', 'run', "#{runit_service_path}/#{service_name}/run"
  queue echo_cmd "chmod +x #{runit_service_path}/#{service_name}/run"

  if stop_template
    # Upload bash script stop service
    set :pid_file, "#{resque_pool_pid}"
    upload_template "#{service_name} stop service", "#{stop_template}", "#{runit_service_path}/#{service_name}/#{stop_template}"
    queue echo_cmd "chmod +x #{runit_service_path}/#{service_name}/#{stop_template}"

    # Upload down file runit service
    set :runit_service_stop, "#{runit_service_path}/#{service_name}/resque-stop.sh"
    upload_template 'resque-pool down service file', 'down', "#{runit_service_path}/#{service_name}/shutdown"
    queue echo_cmd "chmod +x #{runit_service_path}/#{service_name}/shutdown"
  end
end

def start_service(service_name)
  # queue echo_cmd "sudo sv start #{service_name}"
  queue echo_cmd "#{runit_service_path}/#{service_name}/run"
end

def stop_service(service_name)
  # queue echo_cmd "sudo sv stop #{service_name}"
  queue echo_cmd "#{runit_service_path}/#{service_name}/shutdown"
end

def restart_service(service_name)
  # queue echo_cmd "sudo sv start #{service_name}"
  stop_service service_name
  start_service service_name
end
