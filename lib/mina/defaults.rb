####################
# Default parameters
####################
task :defaults do
  set_default :rails_root,                    "#{deploy_to}/#{current_path}"
  set_default :config_path,                   "#{deploy_to}/#{shared_path}/config"
  set_default :sockets_path,                  "#{deploy_to}/#{shared_path}/tmp/sockets"
  set_default :pids_path,                     "#{deploy_to}/#{shared_path}/tmp/pids"
  set_default :config_templates_path,         "lib/mina/templates"
  # Nginx config
  set_default :nginx_server_name,             "#{domain}"
  set_default :nginx_log_path,                "#{deploy_to}/#{shared_path}/log/nginx"
  set_default :nginx_user,                    "www-data"
  set_default :nginx_group,                   "www-data"
  set_default :nginx_config,                  "#{nginx_path}/sites-available/#{app!}.conf"
  set_default :nginx_config_e,                "#{nginx_path}/sites-enabled/#{app!}.conf"
  # Unicorn config
  set_default :unicorn_socket,                "#{sockets_path}/unicorn.sock"
  set_default :unicorn_pid,                   "#{pids_path}/unicorn.pid"
  set_default :unicorn_workers,               2
  # Resque config
  set_default :runit_service_path,            "/etc/runit"
  set_default :resque_pool_service_path,      "resque-pool"
  set_default :resque_scheduler_service_path, "resque-scheduler"
  set_default :resque_scheduler_pid,          "#{pids_path}/resque-scheduler.pid"
  set_default :resque_pool_pid,               "#{pids_path}/resque-pool.pid"
end
