set :domain, 'vagrant_test'
set :branch, 'master'
set :deploy_to, "/var/www/#{app}"
set :user, 'vagrant'
set :group, 'vagrant'
set :nginx_path,          '/etc/nginx'
set :database_adapter, 'postgresql'
set :database_host, 'localhost'
set :database_username, 'postgres'
set :database_password, 'vagrant'
set :database_name, 'app-prod'

set :secret_key_base, '86e4a8f4afe77c45d39739dc5ad728e5d2301c5f72c95d8ae393419ed38acbe3a07a8e8bd24f241e94e95ddb859870b102e46cda8c973414864bda8ab06c51ea'

set :identity_file,   '/Users/hiennguyen/.vagrant.d/insecure_private_key'

# For system-wide RVM install.
#   set :rvm_path, '/usr/local/rvm/bin/rvm'
set :rvm_path, "/usr/local/rvm/scripts/rvm"

# Config mina rsync
set :rsync_options,   ["-e ssh -i #{identity_file}", '--recursive', '--delete', '--delete-excluded']

# Config mina-unicorn
set :unicorn_pid, "#{deploy_to}/#{shared_path}/tmp/pids/unicorn.pid"
set :unicorn_config, "#{deploy_to}/#{shared_path}/config/unicorn.rb"
